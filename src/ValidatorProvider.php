<?php

namespace SemanticLab\Validator;

use Illuminate\Support\ServiceProvider;
use SemanticLab\Validator\Commands\ValidatorMakeCommand;

class ValidatorProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // execute: "php artisan vendor:publish" in cmd could generate user custom config file
        $this->publishes([
            __DIR__ . '/config/validator.php' => config_path('validator.php'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                ValidatorMakeCommand::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // default config file, it's necessary for composer install(update) at first time
        $this->mergeConfigFrom(
            __DIR__ . '/config/validator.php', 'validator'
        );
    }
}
