# Validator
semantic-lab/lara-validator 是一個 Laravel FormRequest 的 php artisan 套件，透過設定 JSON 檔案快速建立 FormRequest 並且 JSON 設定檔也同時供前端開發介接使用.

* [使用方法](#使用方法)
* [指令](#指令)
    * [make](#make)
* [:make](#:make)
    * [設定規則](#設定規則)
        * [基礎設定](#基礎設定)
            * [預設錯誤訊息](#預設錯誤訊息)
            * [客製錯誤訊息](#客製錯誤訊息)
        * [複雜設定](#複雜設定)
            * [巢狀資料設定](#巢狀資料設定)
            * [陣列資料設定](#陣列資料設定)
    * [進階設定](#進階設定)
        * [驗證失敗 response 設定](#驗證失敗-response-設定)
        
## 使用方法
透過 composer:
```bash
composer require semantic-lab/lara-validator --dev
```
或是在你的 composer.json 中加入下面的資訊並從終端機下載本套件:
```JSON
{
    "require-dev": {
        "lara-validator": "1.0"
    }
}
```

## 指令
### make
下列指令會讀取 `config/validators/` 底下的設定檔([看更多細節](#設定規則))，在 `app/Http/Requests` 底下產出 FormRequest。
```bash
php artisan validator:make
```
對於產出來的 FormRequest 我們可以透過 DI 注入至 controller 的 方法中:
```php
<?php

use App\Http\Requests\Login;

class UserController extends Controller
{
    public function login(Login $request) {
        // ...
    }
}
```

## :make
### 設定規則
設定規則的 JSON 的檔案，不只用於 Laravel 產生 FormRequest 同時也可以將這些檔案透過 JavaScript 的 lara-validator ([read more for JavaScript validation](https://www.npmjs.com/package/lara-validator)) 進行驗證.

```JSON
{
  "validators": {}
}
```
上面的範例是最基本(且必須)的設定，我們可以加入若干個 FormRequests 以及要包含的驗證規則。下面是「登入」和「註冊」兩個例子 FormRequest 的例子。
```JSON
{
  "validators": {
    "Login": {
      "body": {}
    },
    "Register": {
      "body": {}
    }
  }
}
```

#### 基礎設定
如果 request body 的資料結構沒有複雜的巢狀資料或是陣列資料，設定上幾乎與 Laravel validation 一樣。要注意的是，所有的驗證規則必須包含於 `body` 底下。

##### 預設錯誤訊息
若是使用 Laravel 預設錯誤訊息，寫法如下:
```JSON
{
  "validators": {
    "Login": {
      "body": {
        "email": "required|string|email",
        "password": "required|string|min:6"
      }
    }
  }
}
```

##### 客製錯誤訊息
如果需要自訂客製錯誤訊息，我們會使用保留字 `rules` 將每個方法以及其對應的錯誤訊息包起來，例如上面範例中的 `password` 欄位，我們要客製錯誤訊息可以改寫成下面的範例。其中如果有些驗證方法要使用原 Laravel 的錯誤訊息，只要將值設為**null**即可。
```json
{
  "validators": {
    "Login": {
      "body": {
        "email": "required|string|email",
        "password": {
          "rules": {
            "required": null,
            "string": null,
            "min:6": "The field 'password' can not be less then :min characters."
          }
        }
      }
    }
  }
}
```

#### 複雜設定
如果驗證資料的結構較為複雜，例如巢狀資料或是陣列資料，請根據下列說明設定

##### 巢狀資料設定
設定巢狀資料其實就如同資料格式一樣，逐一的去設定驗證規則，唯一要注意的是，`rules` 是保留字，如果資料欄未有此字，可能需要修改。
```JSON
{
  "validators": {
    "CreateGroup": {
      "body": {
        "id": "required|string",
        "token": "required|string",
        "group": {
          "isPrivate": "boolean",
          "groupName": "required|string",
          "startDate": "present|date",
          "frequency": "present|in:1,30,90",
          "endDate": "present|date"
        }
      }
    }
  }
}
```
在上面的範例中，我們為 `group` 底下的每一個屬性設定規則。

##### 陣列資料設定
對於陣列資料，我們會以同樣的驗證規則，驗證陣列裡的每一個元素，所以在設定上我們只要設定一次即可。
```JSON
{
  "validators": {
    "CreateUsers": {
      "body": {
        "users": [
          {
            "name": "required|string|min:4",
            "email": "required|email",
            "password": "required|string|min:6|regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i|confirmed",
            "phone": "required|regex:/^(09)[0-9]{8}$/gi|bail",
            "gender": "nullable|numeric|in:0,1,2",
            "address": "present|string"
          }
        ]
      }
    }
  }
}
```
上面範例中，我們撰寫一組驗證規則，驗證 `users` 這個陣列裡面的每一個元素。因此假若陣列中可能存放結構不相同的元素，應該要另外獨立出來。 

### 進階設定
#### 驗證失敗 response 設定
Laravel 驗證錯誤預設會跳轉至首頁，在我們的設定當中，我們支援數個不同的驗證失敗的後續處理。

驗證失敗 response 可以設為全局(同一份 JSON 檔共用)，或是個別 FormRequest 設定特定的處理方法。

例如下面範例中，我們全局預設的處理方法為 `ignore` 但在 Login FormRequest 中，我們特別設定為 `exception` 的處理方法。
```JSON
{
  "validators": {
    "Login": {
      "body": {},
      "failResponse": {
        "type": "exception"
      }
    },
    "CreateUsers": {
      "body": {}
    }
  },
  "failResponse": {
    "type": "ignore"
  }
}
```
下面是我們支援的四種處理方法:

類型       | 說明
-----------|---------
default    | 跳轉至首頁
ignore     | 不做任何處理，request 繼續進入 controller function (action) 中
exception  | 回傳 JSON 格式的 \Illuminate\Support\MessageBag
response   | 回傳時做 IResponse 介面的類別資料 (同樣以 JSON 為回傳格式)

##### Default
在 `failResponse` 的 `type` 設為 "default" 即可，如果式全局皆為 "default" 方法，可以不需設定 `failResponse`。
```JSON
{
  "failResponse": {
    "type": "default"
  }
}
```

##### Ignore
如果我們希望驗證錯誤後仍進到 action 中做額外的處理，只要將 `failResponse` 的 `type` 設為 "ignore"。
```JSON
{
  "failResponse": {
    "type": "ignore"
  }
}
```

##### Exception
如果要將 `\Illuminate\Support\MessageBag` 以 JSON 格式回傳，除了將 `failResponse` 的 `type` 設為 "exception"，另外還可以指定 HTTP status 的狀態，如果沒有設定 Laravel 預設為 422。
```JSON
{
  "failResponse": {
    "type": "exception",
    "httpStatus": 200
  }
}
```

##### Response
如果我們要將錯誤資訊放入特殊的回傳結構中，或是希望在發生錯誤時可以有客製化的動作，首先我們要有一個實作 `app\Http\Response\IValidatorResponse` 的類別。接著在設定檔中，我們要將 `failResponse` 的 `type` 設為 "response"；`class`的部份要只定實作 `IValidatorResponse` 的類別。至於 HTTP status 的設定如 ["Exception"](#exception) 的介紹。

```JSON
{
  "failResponse": {
    "type": "response",
    "class": "LoginResponse",
    "httpStatus": 200
  }
}
```

屬性     | 說明
-------------|:--------------
class        | `app/Http/Response/` 底下的類別
httpStatus   | 要回傳的 HTTP status

In example, we create a Response class which implement App\Http\Responses\IValidatorResponse at first,

下面範例是使用 `app\Http\Responses\manager\LoginResponse.php`，作為 `class` 的設定值。在 JSON 設定檔中，我們只要設為 `manager\LoginResponse` 即可。

```php
<?php
namespace App\Http\Responses\manager;

use App\Http\Responses\IValidatorResponse;

class LoginResponse implements IValidatorResponse
{
    public function setValidationError($error){
        // TODO: Implement setValidationError() method.
    }
    
    public function toJSONResponse($httpStatus = 422){
        // TODO: Implement toJSONResponse() method.
    }
}

```

```json
{
  "validators": {
    "Login": {
      "body": {},
      "failResponse": {
        "type": "response",
        "class": "manager\LoginResponse",
        "httpStatus": 200
      }
    },
    "CreateUsers": {
      "body": {
          // ...
      }
    }
  }
}
```